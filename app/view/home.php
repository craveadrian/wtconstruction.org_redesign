<div id="welcome">
	<p class="welcome-txt">WELCOME</p>
	<div class="row">
		<div class="red-box">
			<div class="text">
				<h1><?php $this->info("company_name"); ?></h1>
				<p>With over 15 years of experience, W&T Construction is your solution for all of your remodel needs. We take great pride in our extreme professionalism, timeliness and superior customer service on every job. We offer high quality services that are tailored to your individual needs and we’re ready to build your dream home for you! Call us today!</p>
				<div class="link-container">
					<div class="lc-left">
						<a href="services#content" class="btn">READ MORE</a>
						<a href="contact#content" class="btn">FREE ESTIMATE</a>
					</div>
					<div class="lc-right">
						<img src="public/images/content/logo1.png" alt="logo 1">
						<img src="public/images/content/logo2.png" alt="logo 2">
						<img src="public/images/content/logo3.png" alt="logo 3">
					</div>
				</div>
			</div>
		</div>
		<img src="public/images/content/welcomeImg.jpg" alt="Bathroom" class="welcome-img">
	</div>
	<p class="what-text">WHAT WE DO</p>
</div>
<div id="services">
	<div class="row">
		<h2>Our Services</h2>
		<div class="service-container">
			<div class="services-box">
				<a href="services#content"><img src="public/images/content/service1.jpg" alt="REMODELING">
				<p>REMODELING <span>SERVICE</span> </p></a>
			</div>
			<div class="services-box">
				<a href="services#content"><img src="public/images/content/service2.jpg" alt="ROOFING">
				<p>ROOFING</p></a>
			</div>
			<div class="services-box">
				<a href="services#content"><img src="public/images/content/service3.jpg" alt="ADDITIONS">
				<p>ADDITIONS</p></a>
			</div>
			<div class="services-box">
				<a href="services#content"><img src="public/images/content/service4.jpg" alt="HANDYMAN">
				<p>HANDYMAN <span>SERVICE</span> </p></a>
			</div>
		</div>
	</div>
</div>
<div id="reviews">
	<h2 class="header-testi">TESTIMONIALS</h2>
	<div class="row">
		<img src="public/images/content/reviewsImg.jpg" alt="reviews image" class="reviews-img">
		<div class="review-box">
			<h2>REVIEWS</h2>
			<p class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
			<p class="comment">Ian and his team did an excellent job on my bathrooms and expect to have the same outcome with my kitchen which he and his team are currently remodeling. The quality of work is excellent. He takes the time to listen to my comments and takes action on them.</p>
			<p class="author">- Suzan G.</p>
			<a href="reviews#content" class="btn">READ MORE</a>
		</div>
	</div>
</div>
<div id="callus">
	<div class="row">
		<h3>We’re ready to build your dream home for you.</h3>
		<h4>CALL US TODAY!</h4>
		<?php $this->info(["phone","tel","phone"]);?>
	</div>
</div>
<div id="contact">
	<h2>Contact Us</h2>
	<div class="row">
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="top">
				<div>
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
				</div>
				<div>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<label><span class="ctc-hide">Location</span>
						<input type="text" name="location" placeholder="Location:">
					</label>
				</div>
			</div>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
			</label>
			<div class="bottom">
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label class="confirmations">
					<input type="checkbox" name="consent" class="consentBox"><span>I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</span>
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label class="confirmations">
					<input type="checkbox" name="termsConditions" class="termsBox"/><span> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></span>
				</label>
				<?php endif ?>
			</div>
			<button type="submit" class="ctcBtn btn2" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
