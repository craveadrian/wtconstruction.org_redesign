<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php if ($view == "home"): ?>
    <link rel="canonical" href="<?php echo URL; ?>" />
	<?php endif; ?>
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hd-left">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
							<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy">PRIVACY POLICY</a></li>
						</ul>
					</nav>
				</div>
				<div class="hd-right">
					<p><?php $this->info(["phone","tel"]); ?></p>
				</div>

			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="slider">
				<img src="public/images/common/sliders/bannerImg1.jpg" alt="Slider 1" class="slider1">
			</div>
			<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Main Logo" class="bannerLogo"> </a>
			<div class="social-media">
				<p>
					<a href="<?php $this->info("fb_link"); ?>" target="_blank">f</a>
					<a href="<?php $this->info("tt_link"); ?>" target="_blank">l</a>
					<a href="<?php $this->info("yt_link"); ?>" target="_blank">x</a>
					<a href="<?php $this->info("rs_link"); ?>" target="_blank">r</a>
				</p>
			</div>
			<div class="row">
				<h1>over  25 years </h1>
				<p>OF CONSTRUCTION EXPERIENCE</p>
				<a href="contact#content" class="btn">FREE ESTIMATE</a>
			</div>
		</div>
	<?php //endif; ?>
